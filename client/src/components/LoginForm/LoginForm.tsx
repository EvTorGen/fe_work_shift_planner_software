import classes from "./LoginForm.module.css"
import Input from "@/components/Input/Input";
import Link from "next/link";
import {useState} from "react";
import BigButton from "@/components/Button/BigButton";
import Grid from "@mui/material/Grid";
import {Box, ButtonGroup, Button, TextField} from "@mui/material";
import Typography from '@mui/material/Typography';

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [notValidEmail, setNotValidEmail] = useState(false);
    const [password, setPassword] = useState('');
    const [notValidPassword, setNotValidPassword] = useState(false);

    const emailHandleChange = (e) => {
        setEmail(e.target.value);
        setNotValidEmail(false);
    };
    const passwordHandleChange = (e) => {
        setPassword(e.target.value);
        setNotValidPassword(false);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
    };
    const fetchData=()=>{
        fetchData('');
    }

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            className={classes.loginPage}>
            <Grid form xs={12} sm={6} md={4}>
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    minHeight="100vh"
                    style={{color: "white"}}
                >
                    <form className={classes.loginForm}>
                        <Typography variant="h6" align='right'>
                            Language
                        </Typography>
                        <Typography variant="h3" align='center'>
                            trask_for identity
                        </Typography>
                        <Typography variant="h6" align='center'>
                            Externí uživatelé Trask
                        </Typography>
                        <Typography variant="h3">
                            Přihlásit se
                        </Typography>

                        <TextField
                            id="filled-basic"
                            label="E-mail"
                            variant="filled"
                            sx={{input: {background: "white"}}}
                            onChange={emailHandleChange}

                        />
                        <TextField
                            id="filled-basic"
                            label="Password"
                            variant="filled"
                            sx={{input: {background: "white"}}}
                            onChange={passwordHandleChange}
                        />
                        <div>
                            <Link href={"/auth/forgotPassword"}>Zapomenuté heslo?</Link>
                        </div>
                        <ButtonGroup orientation="vertical"
                                     aria-label="vertical outlined button group">
                            <Button >Přihlásit se</Button>
                            <Button >Zrušit</Button>
                        </ButtonGroup>
                    </form>
                </Box>
            </Grid>
        </Grid>

    )
};
export default LoginForm;