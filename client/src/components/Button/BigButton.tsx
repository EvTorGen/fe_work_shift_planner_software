import classes from "./Button.module.css"
const BigButton =({onClick, children})=>{
    return(
        <div className={classes.BigButtonDiv}>
        <button onClick={onClick} className={classes.BigButton}>
            {children}
    </button>
</div>
)
}

export default BigButton;