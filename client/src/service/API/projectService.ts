import {fetchData} from "./fetchData";

class ProjectService {

    createNewProject = async (reqBody) => {
        const url = "/projects";
        const requestMethod = "POST";
        const needAccess = true;
        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    getProjects = async () => {
        const url = "/projects";
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    updateProject = async (id, reqBody) => {
        const url = `/projects/${id}`;
        const requestMethod = "PUT";
        const needAccess = true;

        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message;
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    deleteProject = async (id) => {
        const url = `/projects/${id}`;
        const requestMethod = "DELETE";
        const reqBody = false;
        const needAccess = true;
        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message;
        } catch (err) {
            console.log(err)
            throw err
        }
    };
}

const projectService = new ProjectService();
export default projectService;
