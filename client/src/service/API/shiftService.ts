import {fetchData} from "./fetchData";

class ShiftService {

    createNewShift = async (reqBody) => {
        const url = "/shifts";
        const requestMethod = "POST";
        const needAccess = true;
        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    showAllShifts = async () => {
        const url = "/shifts";
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    showCertainShift = async (id) => {
        const url = `/shifts/${id}`;
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    updateCertainShift = async (id, reqBody) => {
        const url = `/shifts/${id}`;
        const requestMethod = "PUT";
        const needAccess = true;

        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message;
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    acceptDeclineShift = async (id, reqBody) => {
        const url = `/shift/accept-decline/${id}`;
        const requestMethod = "PUT";
        const needAccess = true;
        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message;
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    deleteShift = async (id) => {
        const url = `/shifts/${id}`;
        const requestMethod = "DELETE";
        const reqBody = false;
        const needAccess = true;
        try {
            const {message} = await fetchData(url, requestMethod, reqBody, needAccess);
            return message;
        } catch (err) {
            console.log(err)
            throw err
        }
    };
}

const shiftService = new ShiftService();
export default shiftService;
