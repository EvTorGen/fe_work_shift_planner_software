import {fetchData} from "./fetchData";

class UserService {

    createNewUser = async (reqBody) => {
        const url = "/create_new_user";
        const requestMethod = "POST";
        const needAccess = true;
        try {
            const {message, user} = await fetchData(url, requestMethod, reqBody, needAccess);
            return (`${message}, ${user.firstName} ${user.lastName}!`);
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    login = async (reqBody) => {
        const url = "/login";
        const requestMethod = "POST";
        try {
            const {token} = await fetchData(url, requestMethod, reqBody);
            if (!token) {
                throw new Error("No token")
            }
            ;
            localStorage.setItem("token_Shift_Planner", `Bearer ${token}`);
            return ("Successful login!")
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    logout = () => {
        localStorage.removeItem("token_Shift_Planner")
    };
    showUserData = async (id) => {
        const url = `/user/profile/${id}`;
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    getUsers = async () => {
        const url = "/users";
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    getUsersOnProject = async () => {
        const url = "/users/on_project";
        const requestMethod = "GET";
        const reqBody = false;
        const needAccess = true;
        try {
            return await fetchData(url, requestMethod, reqBody, needAccess);
        } catch (err) {
            console.log(err)
            throw err
        }
    };
    editProfile = async (id, reqBody) => {
        const url = `/user/put/${id}`;
        const requestMethod = "PUT";
        const needAccess = true;
        try {
            const {message, user} = await fetchData(url, requestMethod, reqBody, needAccess);
            return (`${message}, ${user.firstName} ${user.lastName}!`);
        } catch (err) {
            console.log(err)
            throw err
        }
    };

    deleteProfile = async (id) => {
        const url = `/user/delete/${id}`;
        const requestMethod = "DELETE";
        const reqBody = false;
        const needAccess = true;
        try {
            const {message, deleteUser} = await fetchData(url, requestMethod, reqBody, needAccess);
            return {message, deleteUser};
        } catch (err) {
            console.log(err)
            throw err
        }
    }
}

const userService = new UserService();
export default userService;
