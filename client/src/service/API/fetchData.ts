import {API_URL} from "@/service/API/config";

export const fetchData = async (url, requestMethod, reqBody = false, needAccess = false) => {
    const URL = `${API_URL}${url}`;
    const headers = () => {
        if (!needAccess) {
            return ({"Content-Type": "application/json"})
        }
        const token = localStorage.getItem("token_Shift_Planner");
        return {
            "Content-Type": "application/json",
            "Authorization": token
        }
    };
    const requestOptions = () => {
        if (!reqBody) {
            return {
                method: requestMethod,
                headers: headers(),
            }
        }
        return {
            method: requestMethod,
            headers: headers(),
            body: JSON.stringify(reqBody)
        }
    };
    const res = await fetch(URL, requestOptions())
        .then((response) => {
            return response.json();
        })
        .catch((err) => {
            throw err
        });
    if (res.error) {
        if (res.errors) {
            throw new Error(`${res.error} because: ${res.errors.errors.map(error => error.msg)}`)
        } else throw new Error(res.error)
    }
    return res;
};