import Head from 'next/head'
import {Inter} from 'next/font/google'
import {useEffect} from 'react'
import {useRouter} from "next/router";

const inter = Inter({subsets: ['latin']})

export default function Home() {
    const router = useRouter();
    useEffect(() => {
        router.push("/auth/login");
    }, [])
    return (
        <>
            <Head>
                <title>Kinder-app</title>
                <meta name="description" content="Kinder-app Trask"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
            </Head>
            <main>

            </main>
        </>
    )
}
