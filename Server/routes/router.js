import express from 'express';
import userController from '../controllers/userController.js';
import shiftController from '../controllers/shiftController.js';
import projectController from '../controllers/projectController.js';

const router = express.Router();

//user routes//////////////////////////////////////////////////
//GET requests
router.get('/users', userController.getUsers);      //only for admin
router.get('/user/profile/:id', userController.showUserData);   //user's profile data
router.get('/users/on_project', userController.getUsersOnProject)  //only for coordinator
//POST requests
router.post('/create_new_user', userController.createNewUser);
router.post('/login', userController.login);
//PUT requests
router.put('/user/put/:id', userController.editProfile);
//Delete requests
router.delete('/user/delete/:id', userController.deleteProfile);

//shift routes//////////////////////////////////////////////////
//GET requests
router.get('/shifts', shiftController.showAllShifts);    //Show all shifts (based on the role)
router.get('/shifts/:id', shiftController.showCertainShift);
//POST requests
router.post('/shifts', shiftController.createNewShift);
//PUT requests
router.put('/shifts/:id', shiftController.updateCertainShift);
router.put('/shift/accept-decline/:id', shiftController.acceptDeclineShift);  //Accept or decline the shift (admin / coordinator)
//Delete requests
router.delete('/shifts/:id', shiftController.deleteShift);

//project routes//////////////////////////////////////////////////
//GET requests
router.get('/projects', projectController.getProjects);
//POST requests
router.post('/projects', projectController.createNewProject);     //only for admin
//PUT requests
router.put('/projects/:id', projectController.updateProject);     //only for admin
//Delete requests
router.delete('/projects/:id', projectController.deleteProject);  //only for admin

export default router;
