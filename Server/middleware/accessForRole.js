import jwt from "jsonwebtoken";
import { secret } from "../config.js";

const accessForRole = (roles) => {
    return (req, res, next) => {
        if (req.method === 'OPTIONS') {
            next()
        }
        try {
            //Token -> headers.authorization (without type - bearer)
            const token = req.headers.authorization.split(' ')[1];
            if (!token) {
                console.log(err);
                return res.status(403).json({ error: "User not logged in" })
            }

            const { role: userRole } = jwt.verify(token, secret.key);
            let hasRole = false;
                if (roles.includes(userRole)) {
                    hasRole = true;
                }
            if (!hasRole) {
                return res.status(403).json({ error: "You do not have an access" })
            }
            next();
        } catch (err) {
            console.log(err);
            return res.status(403).json({ error: "Access error" });
        }
    };
};
export { accessForRole };