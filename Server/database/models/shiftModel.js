import mongoose from "mongoose";

const {Schema} = mongoose;

const shiftSchema = new Schema({
    user_id: {
        type: String,
        ref: 'User',
        required: true
    },
    project_id: {
        type: String,
        ref: 'Project',
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    state: {
        type: String,
        required: true,
        default: "created"
    },
    dateAdded: {
        type: Date,
        default: Date.now
    }
});

const Shift = mongoose.model('Shift', shiftSchema);
export default Shift;