import User from "../database/models/userModel.js";
import { body, validationResult } from "express-validator";
//Bcrypt to hash passwords
import bcrypt from "bcryptjs";
//jsonwebtoken for token generation
import jwt from "jsonwebtoken";
import { secret } from "../config.js";
import { accessForRegistered } from "../middleware/accessForRegistered.js";
import { accessForRole } from "../middleware/accessForRole.js";
import Project from "../database/models/projectModel.js";

//Generation of the access token 
const generateAccessToken = (_id, role, project_id) => {
    const payload = {
        _id,
        role,
        project_id
    }
    return jwt.sign(payload, secret.key)  //No expiration like{ expiresIn: "24h" }
};

class UserController {

    //Registration of a new user
    createNewUser = [
    //Registration check
    accessForRegistered(),
    //Access only for admin
    accessForRole(["admin"]),
        //Validation and sanitization of request body fields
        body("email")
            .trim()
            .isLength({ min: 4, max: 40 })
            .isEmail()
            .withMessage("Email is not valid"),
        body("password")
            .isLength({ min: 3 })
            .withMessage("Password must contain at least 3 characters"),
        body("firstName")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("First name is not specified")
            .isAlphanumeric()
            .withMessage("First name contains non-alphanumeric characters"),
        body("lastName")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("Last name is not specified")
            .isAlphanumeric()
            .withMessage("Last name contains non-alphanumeric characters"),
        body("role")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("Role is not specified")
            .isAlphanumeric()
            .withMessage("Role contains non-alphanumeric characters"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Registration failed", errors })
                }

                const { email, password, firstName, lastName, project_id, role } = req.body;

                //Check if User with this email already exists
                const existingUser = await User.findOne({ email });
                const existingProject = await Project.findById(project_id);
                if (existingUser) {
                    return res.status(400).json({ error: 'User with this email already exists' });
                };
                //Check that the project exists
                    if(!existingProject) {
                    return res.status(400).json({error: 'Project was not found'})
                    };
                    //Hash a password
                    const saltRounds = 5;
                    const hashPassword = bcrypt.hashSync(password, saltRounds);

                    //Create new User
                    const user = new User({ email, password: hashPassword, firstName, lastName, role, project_id });
                    await User.create(user);
                    return res.json({ message: 'Successful registration', user });

            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'Registration error' })
            }
        }
    ];


    //login of Registered user 
    login = [
        //Validation and sanitization of request body fields
        body("email")
            .trim()
            .isLength({ min: 4, max: 40 })
            .isEmail()
            .withMessage("Email is not valid"),
        body("password")
            .isLength({ min: 3 })
            .withMessage("Password must contain at least 3 characters"),
        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Wrong email or password format", errors })
                }
                const { email, password } = req.body;

                //Check if User with this email exists
                const userLogin = await User.findOne({ email });
                if (!userLogin) {
                    return res.status(400).json({ error: `User with email ${email} was not found` })
                };

                //Compare password with valid hashed password using bcrypt
                const validPassword = bcrypt.compareSync(password, userLogin.password);
                if (!validPassword) {
                    return res.status(400).json({ error: "Wrong password" })
                }

                //Generate jwt token
                const token = generateAccessToken(userLogin._id, userLogin.role, userLogin.project_id);
                return res.json({ token });
            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'Login error' })
            }

        }
    ];

    showUserData = [
        //Registration check
        accessForRegistered(),
        async (req, res) => {
            try {
                const userToken = req.user;
                const userData = await User.findById(req.params.id);

                if (!userData) {
                    return res.status(400).json({ error: "User data was not found" });
                }
                else
                {
                    if((req.params.id !== userToken._id) && (userToken.role !== 'admin') && (userToken.role !== 'coordinator'))
                    {return res.status(400).json({ error: "You do not have an access" })}
                }
                    return res.json(userData);

            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'Get request error, process failed' })
            }
        }

    ];
    //GET all registered users
    getUsers = [
        //Registration check
        accessForRegistered(),
        //Access only for admin
        accessForRole(["admin"]),
        async (req, res) => {
            try {
                const userList = await User.find();
                res.json(userList)
            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'GET request error' })
            }
        }];
    //Get users on project
    getUsersOnProject = [
        //Registration check
        accessForRegistered(),
        //Access only for coordinator
        accessForRole(["coordinator"]),

        async (req, res) => {
            const userToken = req.user;
            try {
                const userList = await User.find();
                const usersOnProjectList = userList.filter(user =>
                    user.project_id === userToken.project_id
                )
                res.json(usersOnProjectList)
            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'GET request error' })
            }
        }];

    //Edit profile of the registered user
    editProfile = [

        //Registration check
        accessForRegistered(),
        //Validation and sanitization of request body fields
        body("email")
            .trim()
            .isLength({ min: 4, max: 40 })
            .isEmail()
            .withMessage("Email is not valid"),
        body("password")
            .isLength({ min: 3 })
            .withMessage("Password must contain at least 3 characters"),
        body("firstName")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("First name is not specified")
            .isAlphanumeric()
            .withMessage("First name contains non-alphanumeric characters"),
        body("lastName")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("Last name is not specified")
            .isAlphanumeric()
            .withMessage("Last name contains non-alphanumeric characters"),
        body("role")
            .trim()
            .isLength({ min: 1 })
            .escape()
            .withMessage("Role is not specified")
            .isAlphanumeric()
            .withMessage("Role contains non-alphanumeric characters"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Update failed", errors })
                }

                const { email, password, firstName, lastName, project_id, role } = req.body;

                //Check if User with this email already exists --- except registered user's email --- if admin - no check!
                const existingUser = await User.findOne({ email });
                const userToken = req.user;
                const currentUser = await User.findById(userToken._id);
                const existingProject = await Project.findById(project_id);
                if (existingUser && (existingUser.email !== currentUser.email) && (currentUser.role !== 'admin')) {
                    return res.status(400).json({ error: 'User with this email already exists / You do not have an access ' })
                };
                //Check that the project exists
                    if (!existingProject) {
                        return res.status(400).json({error: 'Project was not found'})
                    };
                        //Hash a password
                        const saltRounds = 5;
                        const hashPassword = bcrypt.hashSync(password, saltRounds);

                        //Update User
                        const user = await User.findByIdAndUpdate(req.params.id, {
                            email,
                            password: hashPassword,
                            firstName,
                            lastName,
                            role,
                            project_id
                        }, {new: true});
                        return res.json({message: 'Successful update', user});
            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'Put request error, Update failed' })
            }
        }
    ];



    deleteProfile = [
        //Registration check
        accessForRegistered(),
        //Access only for admin
        accessForRole(["admin"]),
        async (req, res) => {
            try {
                const deleteUser = await User.findByIdAndDelete(req.params.id);
                if (!deleteUser) {
                    return res.status(400).json({ error: "User was not found" })
                }
                else
                    return res.json({ message: "User was deleted", deleteUser })
            } catch (err) {
                console.log(err);
                res.status(400).json({ error: 'Delete request error, process failed' })
            }
        }
    ];
};

const userController = new UserController();

export default userController;