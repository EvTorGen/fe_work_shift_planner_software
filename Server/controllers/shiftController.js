import Shift from "../database/models/shiftModel.js";
import Project from "../database/models/projectModel.js";

import { body, validationResult } from "express-validator";
import { accessForRegistered } from "../middleware/accessForRegistered.js";
import { accessForRole } from "../middleware/accessForRole.js";

class ShiftController {
    createNewShift = [
        //Registration check
        accessForRegistered(),
        //Access only for tester
        accessForRole(["tester"]),
        //Validation and sanitization of request body fields
        body("startDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect start Date format"),
        body("endDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect end Date format"),
        body("state")
            .optional({ checkFalsy: true }),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Failed to create new shift", errors })
                };

                const userToken = req.user;
                const { startDate, endDate, state } = req.body;
                const existingProject = await Project.findById(userToken.project_id);
                if(!existingProject){
                    return res.status(400).json({error: 'Project was not found'})}
                else{
                const shift = new Shift({ user_id: userToken._id, startDate, endDate, state, project_id: existingProject._id });
                await Shift.create(shift);
                return res.json({ message: "Shift successfully created"})}
            } catch (err) {
                res.status(400).json({ error: "Post request error" })
            }
        }];

    showAllShifts= [
        //Registration check
        accessForRegistered(),
        //Get request
        async (req, res) => {
            try {
                const userToken = req.user;
                //Find all shifts associated with User
                const shifts = await Shift.find();
                let associatedShifts = null;
                if(userToken.role === "admin")
                {associatedShifts = shifts};
                if(userToken.role === 'coordinator')
                {associatedShifts = shifts.filter(shift=> shift.project_id === userToken.project_id)};
                if(userToken.role === 'tester')
                {associatedShifts = shifts.filter(shift=> shift.user_id === userToken._id)};

                return res.json(associatedShifts);

            } catch (err) {
                res.status(400).json({ error: "GET request error" })
            }
        }
    ];

    showCertainShift = [
        //Registration check
        accessForRegistered(),
        //Get request
        async (req, res) => {
            try {
                const shift = await Shift.findById(req.params.id);
                if (shift) {
                    return res.json(shift);
                } else {
                    return res.status(404).json({ error: "Shift was not found" })
                }

            } catch (err) {
                res.status(400).json({ error: "GET request error. Shift with this id was not found." })
            }
        }
    ];

    updateCertainShift = [
        //Registration check
        accessForRegistered(),

        //Validation and sanitization of request body fields
        body("startDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect start Date format"),
        body("endDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect end Date format"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Failed to update the shift", errors })
                };

                const userToken = req.user;
                const currentShift = await Shift.findById(req.params.id);
                if(currentShift.user_id !== userToken._id){
                    return res.status(400).json({ error: "You do not have an access to update the shift"})
                };
                const { startDate, endDate} = req.body;
                const initialState = "created";
                await Shift.findByIdAndUpdate(req.params.id, { startDate, endDate, state:initialState }, { new: true });

                return res.json({ message: "Shift successfully updated " })
            } catch (err) {
                res.status(400).json({ error: "Put request error" })
            }
        }];

    acceptDeclineShift = [
        //Registration check
        accessForRegistered(),
        //Access only for coordinator or admin
        accessForRole(["coordinator", "admin"]),
        //Validation and sanitization of request body fields
        body("state")
            .trim()
            .isLength({ min: 1, max: 15 })
            .escape()
            .withMessage("State is not specified")
            .isAlphanumeric()
            .withMessage("State contains non-alphanumeric characters"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Failed to update the shift", errors })
                };
                const {state} = req.body;
                await Shift.findByIdAndUpdate(req.params.id, { state }, { new: true });
                if(state === "accepted")
                {return res.json({ message: "Shift successfully accepted. "})};
                if(state === "declined")
                {return res.json({ message: "Shift successfully declined. "})}
                else{
                    return res.json({ message: "Shift successfully updated. "})
                }
            } catch (err) {
                res.status(400).json({ error: "Put request error" })
            }
        }
    ];

    deleteShift = [
        //Registration check
        accessForRegistered(),
        //Access only for tester
        accessForRole(["tester"]),
        async (req, res) => {

            try {
                //Check if user is the creator of the shift
                const userToken = req.user;
                const currentShift = await Shift.findById(req.params.id);
                if (userToken._id !== currentShift.user_id) {
                    return res.status(400).json({ error: "You do not have a permission" });
                };
                //Tester cannot delete accepted/declined shift
                if (currentShift.state !== "created") {
                    return res.status(400).json({ error: "You cannot delete accepted/declined shift" });
                };
                await Shift.findByIdAndDelete(req.params.id);
                    return res.json({ message: "Shift successfully deleted"})
            } catch (err) {
                res.status(400).json({ error: "Delete request error" })
            }
        }
    ];
};

const shiftController = new ShiftController();
export default shiftController;
