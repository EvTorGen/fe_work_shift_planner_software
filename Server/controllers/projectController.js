import Project from "../database/models/projectModel.js";
import { body, validationResult } from "express-validator";
import { accessForRole } from "../middleware/accessForRole.js";
import { accessForRegistered } from "../middleware/accessForRegistered.js";

class ProjectController {

    //Create new project
    createNewProject = [
        //Registration check
        accessForRegistered(),
        //Access only for admin
        accessForRole(["admin"]),
        //Validation and sanitization of request body fields
        body("startDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect start Date format"),
        body("endDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect end Date format"),
        body("title")
            .trim()
            .isLength({ min: 1, max: 30 })
            .escape()
            .withMessage("Title of the project is not specified")
            .isAlphanumeric()
            .withMessage("Title of the project contains non-alphanumeric characters"),
        body("note")
            .trim()
            .isLength({ min: 1, max: 30 })
            .escape()
            .withMessage("Note is not specified"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Failed to create a new project", errors })
                }
                const { title } = req.body;
                //Check if the project already exists
                const existingProject = await Project.findOne({ title });
                if (existingProject) {
                    return res.status(400).json({ error: "Project already exists" });
                }
                await Project.create(req.body);
                return res.json({ message: "A new project was successfully created"});
            } catch (err) {
                res.status(400).json({ error: "Failed to create a new project" });
            }
        }
    ];
    //Show all projects
    getProjects = [
        //Registration check
        accessForRegistered(),
        async (req, res) => {
            try {
                const projectList = await Project.find();
                return res.json(projectList);
            } catch (err) {
                res.status(400).json({ error: "Failed to get the list of projects" })
            }
        }
    ];
    //Update project
    updateProject =[
        //Registration check
        accessForRegistered(),
        //Access only for admin
        accessForRole(["admin"]),
        //Validation and sanitization of request body fields
        body("startDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect start Date format"),
        body("endDate")
            .isISO8601()
            .toDate()
            .withMessage("Incorrect end Date format"),
        body("title")
            .trim()
            .isLength({ min: 1, max: 30 })
            .escape()
            .withMessage("Title of the project is not specified")
            .isAlphanumeric()
            .withMessage("Title of the project contains non-alphanumeric characters"),
        body("note")
            .trim()
            .isLength({ min: 1, max: 30 })
            .escape()
            .withMessage("Note is not specified"),

        //Request process after validation
        async (req, res) => {
            try {
                //Find and show validation errors
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(400).json({ error: "Failed to create a new project", errors })
                }
                const { title, note, startDate, endDate } = req.body;
                //Check if the project already exists
                const existingProject = await Project.findOne({ title });
                if (existingProject._id !== req.params.id) {
                    return res.status(400).json({ error: "Project already exists" });
                }
                await Project.create(req.body);
                return res.json({ message: "Project was successfully updated"});
            } catch (err) {
                res.status(400).json({ error: "Failed to create a new project" });
            }
        }
    ]

    //Delete project
    deleteProject = [
        //Registration check
        accessForRegistered(),
        //Access only for admin
        accessForRole(["admin"]),
        async (req, res) => {
            try {
                const deleted = await Project.findByIdAndDelete(req.params.id);
                return res.json({ message: "Project was successfully deleted", deleted })
            } catch (err) {
                res.status(400).json({ error: "Failed to delete the project" })
            }
        }];

}

const projectController = new ProjectController();

export default projectController;